package az.ingress.kafkams6;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.core.KafkaTemplate;

@SpringBootApplication
@RequiredArgsConstructor
public class KafkaMs6Application implements CommandLineRunner {

    private final KafkaTemplate<String, String> kafkaTemplate;

    public static void main(String[] args) {
        SpringApplication.run(KafkaMs6Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        int i = 0;
        while (true) {
            i++;
            Thread.sleep(1000);
            kafkaTemplate.send("my-first-kafka-topic", "key1" + i, "Hello Farid!");
        }
    }
}
